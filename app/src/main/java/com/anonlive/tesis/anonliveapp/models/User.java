package com.anonlive.tesis.anonliveapp.models;

import com.google.firebase.database.IgnoreExtraProperties;

// [START blog_user_class]
@IgnoreExtraProperties
public class User {
    public String nombre;
    public String username;
    public String correo;
    public String nombre_anonimo;
    public boolean modo_anonimo;
    

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public User(String username, String correo) {//Default Access with correo and password
        this.username = username;
        this.correo = correo;
    }
    public User(String nombre,String username, String correo) {
        this.nombre=nombre;
        this.username = username;
        this.correo = correo;
    }
    public User(String nombre,String username, String correo,String nombre_anonimo, boolean modo_anonimo) {
        this.nombre=nombre;
        this.username = username;
        this.correo = correo;
        this.nombre_anonimo=nombre_anonimo;
        this.modo_anonimo=modo_anonimo;
    }


}
// [END blog_user_class]
