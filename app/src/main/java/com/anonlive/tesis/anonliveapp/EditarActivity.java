package com.anonlive.tesis.anonliveapp;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.anonlive.tesis.anonliveapp.models.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditarActivity extends BaseActivity {

    @Bind(R.id.editTxtNombre)
    EditText editTxtNombre;
    @Bind(R.id.editTextTelefono)
    EditText editTextTelefono;
    @Bind(R.id.editTextCorreo)
    EditText editTextCorreo;
    @Bind(R.id.editTxtNombreAnonimo)
    EditText editTxtNombreAnonimo;
    @Bind(R.id.editTextCorreoAnonimo)
    EditText editTextCorreoAnonimo;
    @Bind(R.id.buttonActualizarPerfil)
    Button buttonActualizarPerfil;
    @Bind(R.id.fab)
    FloatingActionButton fab;

    DatabaseReference database;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.toolbar_layout)
    CollapsingToolbarLayout toolbarLayout;
    @Bind(R.id.app_bar)
    AppBarLayout appBar;
    @Bind(R.id.wrapperNombre)
    TextInputLayout wrapperNombre;
    @Bind(R.id.wrapperTelefono)
    TextInputLayout wrapperTelefono;
    @Bind(R.id.wrapperCorreo)
    TextInputLayout wrapperCorreo;
    @Bind(R.id.textView2)
    TextView textView2;
    @Bind(R.id.wrapperNombreAnonimo)
    TextInputLayout wrapperNombreAnonimo;
    @Bind(R.id.wrapperCorreoAnonimo)
    TextInputLayout wrapperCorreoAnonimo;
    private StorageReference mStorageRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        database = FirebaseDatabase.getInstance().getReference();
        mStorageRef = FirebaseStorage.getInstance().getReference();

        final String userId = getUid();
        database.child("users").child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                editTxtNombre.setText(user.nombre);
                editTextCorreo.setText(user.correo);
                editTxtNombreAnonimo.setText(user.nombre_anonimo);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    //metodo de datos del perfil
    private void updateProfile() {
        Map<String, Object> userData = new HashMap<>();
        userData.put("correo", editTextCorreo.getText().toString());
        userData.put("nombre", editTxtNombre.getText().toString());
        userData.put("nombre_anonimo", editTxtNombreAnonimo.getText().toString());
        database.child("users").child(getUid()).updateChildren(userData);

    }

    //Boton de actualizar el perfil
    @OnClick(R.id.buttonActualizarPerfil)
    public void onClick() {
        updateProfile();
        Toast.makeText(EditarActivity.this,"Guardando...",Toast.LENGTH_SHORT).show();
        finish();
    }
}
