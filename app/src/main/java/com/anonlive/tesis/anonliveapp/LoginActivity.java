/*
 *
 *  * Copyright (C) 2014 Antonio Leiva Gordillo.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  **
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package com.anonlive.tesis.anonliveapp;



import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class LoginActivity extends BaseActivity {
    private static final String TAG = "user";
    @Bind(R.id.progress)
    ProgressBar progress;
    @Bind(R.id.btnRegistrar)
    Button btnRegistrar;
    @Bind(R.id.btnIngresar)
    Button btnIngresar;
    @Bind(R.id.password)
    EditText password;
    @Bind(R.id.username)
    EditText username;
    @Bind(R.id.activity_login)
    RelativeLayout container;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    private DatabaseReference database;//My Database Reference


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    onAuthSuccess(user);
                    Log.d(TAG, "Sesion iniciada:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "No hay ninguna Sesion Iniciada");
                }
                // ...
            }
        };
        database = FirebaseDatabase.getInstance().getReference();

    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    public void showProgress() {
        progress.setVisibility(View.VISIBLE);
    }


    public void hideProgress() {
        progress.setVisibility(View.GONE);
    }


    public void setUsernameError() {
        username.setError(getString(R.string.username_error));
    }


    public void setPasswordError() {
        password.setError(getString(R.string.password_error));
    }


    public void lanzarRegistrar(View view) {
        Intent i = new Intent(this, RegisterActivity.class);
        startActivity(i);
    }


    @OnClick({R.id.btnRegistrar, R.id.btnIngresar})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnRegistrar:
                lanzarRegistrar(null);
                break;
            case R.id.btnIngresar:
                sigInUser(username.getText().toString(), password.getText().toString());
                break;
        }
    }

    private void onErrorLogin(String error) {
        Snackbar.make(container, error, Snackbar.LENGTH_SHORT).show();

    }

    public void setupInput(boolean enable) {
        password.setEnabled(enable);
        username.setEnabled(enable);
        btnIngresar.setEnabled(enable);
        btnRegistrar.setEnabled(enable);
    }

    private void sigInUser(String email, String passw) {

        if (!validateForm()) {
            return;
        }
        showProgressDialog();
        mAuth.signInWithEmailAndPassword(email, passw)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithEmail:onComplete:" + task.isSuccessful());
                        if (task.isSuccessful()) {
                            hideProgressDialog();
                            onAuthSuccess(task.getResult().getUser());
                        }
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithEmail", task.getException());
                            hideProgressDialog();
                            inputSetupDefault();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                errorLogin(e.getMessage());
            }
        });
    }

    private void onAuthSuccess(FirebaseUser user) {
        startActivity(new Intent(LoginActivity.this, NavigationActivity.class));
        finish();
    }

    private void errorLogin(String mensaje) {
        Snackbar.make(container, mensaje, Snackbar.LENGTH_SHORT).show();

    }

    private boolean validateForm() {
        boolean result = true;
        if (TextUtils.isEmpty(username.getText().toString().trim())) {
            username.setError(getString(R.string.username_error));
            result = false;
        } else {
            username.setError(null);
        }
        if (TextUtils.isEmpty(password.getText().toString().trim())) {
            password.setError(getString(R.string.password_error));
        }
        return result;
    }
    private void inputSetupDefault(){
        password.setText("");
    }


}


















