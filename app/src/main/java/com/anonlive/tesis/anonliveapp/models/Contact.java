package com.anonlive.tesis.anonliveapp.models;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by Fhidel Villanueva on 5/12/2016.
 */

public class Contact {
    public String uid;
    public String name_contact;
    public String email_contact;
    public boolean mode_contact;

    //[Default Construct to Use]
    public Contact(){


    }
    public Contact(String uid,String name_contact,String email_contact){
        this.uid=uid;
        this.name_contact=name_contact;
        this.email_contact=email_contact;
    }
    public Map<String, Object> toMap(){
        Map<String, Object> results=new HashMap<>();
        results.put("uid",uid);
        results.put("name_contac",name_contact);
        results.put("email_contact",email_contact);
        results.put("mode_contac",mode_contact);
        return results;
    }
}
