package com.anonlive.tesis.anonliveapp.viewholder;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import com.anonlive.tesis.anonliveapp.R;
import com.anonlive.tesis.anonliveapp.models.Contact;

/**
 * Created by Fhidel Villanueva on 9/12/2016.
 */

public class ContactViewHolder extends RecyclerView.ViewHolder {

    TextView name_contatc;
    FloatingActionButton addContact;

    public ContactViewHolder(View itemView) {
        super(itemView);
        name_contatc=(TextView)itemView.findViewById(R.id.post_author);
        addContact=(FloatingActionButton)itemView.findViewById(R.id.floatingAddContact);
    }
    public void bindToContact(Contact contact,View.OnClickListener onClickListener){
        name_contatc.setText(contact.name_contact);
        addContact.setOnClickListener(onClickListener);
    }
}
