package com.anonlive.tesis.anonliveapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.anonlive.tesis.anonliveapp.models.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class RegisterActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "LOG FIREBASE";

    @Bind(R.id.editTxtNombre)
    EditText editTxtNombre;
    @Bind(R.id.editTxtEmail)
    EditText editTxtEmail;
    @Bind(R.id.ediTextPassword)
    EditText ediTextPassword;
    @Bind(R.id.crear_cuenta)
    protected Button button;
    @Bind(R.id.activity_register)
    RelativeLayout container;


    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    DatabaseReference database;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };
        database = FirebaseDatabase.getInstance().getReference();
    }

    @Override
    public void onClick(View view) {

    }

    @OnClick(R.id.crear_cuenta)
    public void onClick() {
        createUser(editTxtEmail.getText().toString(), ediTextPassword.getText().toString());
    }
    private void createUser(final String email, final String password) {
        if(!validateForm()){
            return;
        }
        showProgressDialog();
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());
                        if (task.isSuccessful()) {
                            hideProgressDialog();
                            onAuthSuccess(task.getResult().getUser());
                            sigInUser(email, password);
                        }
                        if (!task.isSuccessful()) {
                            errorCreate("Error !!");
                        }

                        // ...
                    }
                });
    }

    public void navigateCorrectCreateUser() {
        Intent intent = new Intent(this, NavigationActivity.class);
        startActivity(intent);

    }

    private void successCreate(String msg) {
        Snackbar.make(container, msg, Snackbar.LENGTH_SHORT).show();
    }

    private void errorCreate(String msg) {
        Snackbar.make(container, msg, Snackbar.LENGTH_SHORT).show();
    }

    private void sigInUser(String email, String passw) {

        mAuth.signInWithEmailAndPassword(email, passw)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithEmail:onComplete:" + task.isSuccessful());
                        if (task.isSuccessful()) {
                        }
                        if (!task.isSuccessful()) {
                        }

                        // ...
                    }
                });


    }
    private boolean validateForm() {
        boolean result = true;
        if (TextUtils.isEmpty(editTxtNombre.getText().toString().trim())) {
            editTxtNombre.setError(getString(R.string.username_error));
            result = false;
        } else {
            editTxtNombre.setError(null);
        }

        if (TextUtils.isEmpty(ediTextPassword.getText().toString().trim())) {
            ediTextPassword.setError(getString(R.string.password_error));
            result=false;
        }else{
            ediTextPassword.setError(null);
        }
        if (TextUtils.isEmpty(editTxtEmail.getText().toString().trim())) {
            editTxtEmail.setError(getString(R.string.email_error));
            result=false;
        }else{
            editTxtEmail.setError(null);
        }
        return result;
    }
    private void onAuthSuccess(FirebaseUser user) {
        String username = usernameFromEmail(user.getEmail());
        String name=editTxtNombre.getText().toString().trim();

        writeNewUser(user.getUid(),name, username, user.getEmail(),"anonimo",false);
        startActivity(new Intent(RegisterActivity.this, NavigationActivity.class));
        finish();
    }
    private String usernameFromEmail(String email) {
        if (email.contains("@")) {
            return email.split("@")[0];
        } else {
            return email;
        }
    }
    private void writeNewUser(String userId, String name,String username, String email,String name_anonimo, boolean mode) {
        User user = new User(name,username, email,name_anonimo,mode);
        database.child("users").child(userId).setValue(user);
    }


    private void errorLogin(String s) {
    }

    private void succesfullLogin(String s) {
    }


    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

}
